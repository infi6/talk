#include "Connection.h"

Connection::Connection() : socket(new QTcpSocket())
{
    QObject::connect(socket, SIGNAL(connected()),this, SLOT(connected()));
    QObject::connect(socket, SIGNAL(disconnected()),this, SLOT(disconnected()));
    QObject::connect(socket, SIGNAL(readyRead()),this, SLOT(readyRead()));

    this->isLogoutByUser = false;
}

Connection::~Connection()
{
    delete socket;
}

void Connection::connect(QString address, qint16 port, QString nickname, QString password, bool isNewUser)
{
    this->nickname = nickname;
    this->password = password;
    this->isNewUser = isNewUser;
    this->isLogoutByUser = false;

    socket->connectToHost(address, port);

    if(!socket->waitForConnected(2500))
    {
        QMessageBox::critical(NULL, "Error!", "Cannot connect to server!");
    }
}

void Connection::disconnect(QString result)
{
    MessageEncoder *encoder = new MessageEncoder();
    QString message = encoder->Encode({new ServerInfo("nick", this->nickname), new ServerInfo("disconnect", result)});

    writeToServer(message);
    socket->disconnectFromHost();

    delete encoder;
}

void Connection::connected()
{
    MessageEncoder *encoder = new MessageEncoder();
    QString isNewUserText = isNewUser ? "true" : "false";
    QString message = encoder->Encode({new ServerInfo("nick", this->nickname), new ServerInfo("pass", this->password), new ServerInfo("newuser", isNewUserText)});

    writeToServer(message);

    this->isConnected = true;
    emit changeConnectionStatus();

    delete encoder;
}

void Connection::disconnected()
{
    this->isConnected = false;
    emit changeConnectionStatus();

    if(!this->isLogoutByUser)
    {
        emit serverDisconnected();
    }
}

void Connection::readyRead()
{
    QString message = socket->readAll();
    qDebug() << "FROM SERVER: " + message;

    MessageDecoder *decoder = new MessageDecoder();
    QStringList correctMessages = decoder->SplitMessage(message);

    for(int i = 0; i < correctMessages.size(); i++)
    {
        QVector<ServerInfo*> messageDecoded = decoder->Decode(correctMessages[i]);

        if(messageDecoded[1]->getKeyword() == "status")
        {
            if(messageDecoded[1]->getContent() == "successful")
            {
                MessengerWindow *messengerwindow = new MessengerWindow(NULL, this);
                messengerwindow->show();
            }
            else if(messageDecoded[1]->getContent() == "exist")
            {
                QMessageBox::critical(NULL, "Info!", "Given nickname is already taken!");
                this->disconnect("false");
                this->isLogoutByUser = true;
            }
            else if(messageDecoded[1]->getContent() == "alreadylogged")
            {
                QMessageBox::critical(NULL, "Info!", "User is already logged!");
                this->disconnect("false");
                this->isLogoutByUser = true;
            }
            else if(messageDecoded[1]->getContent() == "wrongpassword")
            {
                QMessageBox::critical(NULL, "Info!", "Wrong password!");
                this->disconnect("false");
                this->isLogoutByUser = true;
            }
        }
        else if(messageDecoded[1]->getKeyword() == "addfriend")
        {
            QString friendNickname = messageDecoded[2]->getContent();
            emit changeFriendStatus(messageDecoded[1]->getContent(), friendNickname);
        }
        else if(messageDecoded[1]->getKeyword() =="message")
        {
            QString from = messageDecoded[0]->getContent();
            QString message = messageDecoded[1]->getContent();
            QString to = messageDecoded[2]->getContent();
            emit newMessage(from, message, to);
        }
    }

    delete decoder;
}

bool Connection::getIsConnected()
{
    return this->isConnected;
}

void Connection::writeToServer(QString message)
{
    socket->write(message.toStdString().c_str());
    qDebug() << "TO SERVER " + message;
}

QString Connection::getNickname()
{
    return this->nickname;
}

QString Connection::getRecieverNickname()
{
    return this->recieverNickname;
}

void Connection::setRecievierNickname(QString recieverNickname)
{
    this->recieverNickname = recieverNickname;
}

void Connection::logoutByUser()
{
    this->isLogoutByUser = true;
}
