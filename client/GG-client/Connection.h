#ifndef CONNECTION_H
#define CONNECTION_H

#include "Dependencies.h"

class Connection : public QObject
{
    Q_OBJECT

public:
    Connection();
    ~Connection();

    void connect(QString address, qint16 port, QString nickname, QString password, bool isNewUser);
    void disconnect(QString result);
    QString getNickname();
    bool getIsConnected();
    void writeToServer(QString messages);

    QString getRecieverNickname();
    void setRecievierNickname(QString recieverNickname);

    void logoutByUser();
    bool getIsLogoutByUser();

private:
    QTcpSocket *socket;
    QString nickname;
    QString password;
    QString recieverNickname;
    bool isNewUser;
    bool isConnected;
    bool isLogoutByUser;

private slots:
    void connected();
    void disconnected();
    void readyRead();

signals:
    void changeConnectionStatus();
    void changeFriendStatus(QString status, QString nickname);
    void newMessage(QString from, QString message, QString to);
    void serverDisconnected();
};

#endif // CONNECTION_H
