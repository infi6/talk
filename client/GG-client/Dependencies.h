#ifndef DEPENDENCIES_H
#define DEPENDENCIES_H

#include <QtNetwork>
#include <QMessageBox>
#include <QTcpSocket>
#include <QAbstractSocket>
#include <QDebug>
#include <QList>
#include <QString>
#include <QObject>
#include <QVector>
#include <QCloseEvent>
#include <QApplication>
#include <QKeyEvent>

#include <vector>

#include "Connection.h"
#include "ServerInfo.h"
#include "MessageDecoder.h"
#include "MessageEncoder.h"
#include "messengerwindow.h"

using namespace std;

#endif // DEPENDENCIES_H
