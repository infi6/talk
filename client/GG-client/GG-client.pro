#-------------------------------------------------
#
# Project created by QtCreator 2018-01-02T11:24:38
#
#-------------------------------------------------

QT       += core gui
QT       += network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = GG-client
TEMPLATE = app


SOURCES +=\
    MessageEncoder.cpp \
    ServerInfo.cpp \
    Connection.cpp \
    Main.cpp \
    MessageDecoder.cpp \
    messengerwindow.cpp \
    mainwindow.cpp

HEADERS  += \
    MessageDecoder.h \
    MessageEncoder.h \
    ServerInfo.h \
    Connection.h \
    Dependencies.h \
    messengerwindow.h \
    mainwindow.h

FORMS    += \
    messengerwindow.ui \
    mainwindow.ui

CONFIG   += c++11
