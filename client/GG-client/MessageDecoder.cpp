#include "MessageDecoder.h"

MessageDecoder::MessageDecoder(){}
MessageDecoder::~MessageDecoder(){}

QVector<ServerInfo*> MessageDecoder::Decode(QString message)
{
    QStringList splitted = message.split("#");

    QVector<ServerInfo*> serverInfos;

    for(int i = 0; i < splitted.length(); i++)
    {
        if(splitted[i] != "" && splitted[i] != "~")
        {
            ServerInfo *serverInfo = new ServerInfo(splitted[i]);
            serverInfos.push_back(serverInfo);
        }
    }

    return serverInfos;
}

QStringList MessageDecoder::SplitMessage(QString message)
{
    QStringList allMessage =  message.split("~");
    QStringList correctMessage;

    for(int i = 0; i < allMessage.size(); i++)
    {
        if(allMessage[i] != "")
        {
            correctMessage << allMessage[i];
        }
    }

    return correctMessage;
}
