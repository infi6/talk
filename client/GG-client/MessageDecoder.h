#ifndef MESSAGEDECODER_H
#define MESSAGEDECODER_H

#include "Dependencies.h"

class ServerInfo;

class MessageDecoder
{
public:
    MessageDecoder();
    ~MessageDecoder();

    QVector<ServerInfo*> Decode(QString message);
    QStringList SplitMessage(QString message);
};

#endif // MESSAGEDECODER_H
