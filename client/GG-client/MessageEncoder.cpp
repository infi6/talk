#include "MessageEncoder.h"

MessageEncoder::MessageEncoder(){}
MessageEncoder::~MessageEncoder(){}

QString MessageEncoder::Encode(QVector<ServerInfo*> serverInfos)
{
    QString msg = "#";

    for(auto &serverInfo : serverInfos)
    {
        msg += serverInfo->join() + "#";
    }

    return msg + "~";
}
