#ifndef MESSAGEENCODER_H
#define MESSAGEENCODER_H

#include "Dependencies.h"

class ServerInfo;

class MessageEncoder
{
public:
    MessageEncoder();
    ~MessageEncoder();

    QString Encode(QVector<ServerInfo*> serverInfos);
};

#endif // MESSAGEENCODER_H
