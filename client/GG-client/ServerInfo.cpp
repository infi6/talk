#include "ServerInfo.h"

ServerInfo::ServerInfo(QString message)
{
    QStringList splitted = message.split(":");
    this->keyword = splitted[0];
    this->content = splitted[1];
}

ServerInfo::ServerInfo(QString keyword, QString content)
{
    this->keyword = keyword;
    this->content = content;
}

QString ServerInfo::getKeyword()
{
    return this->keyword;
}

void ServerInfo::setKeyword(QString keyword)
{
    this->keyword = keyword;
}

QString ServerInfo::getContent()
{
    return this->content;
}

void ServerInfo::setContent(QString content)
{
    this->content = content;
}

QString ServerInfo::toString()
{
    return "Keyword: " + this->keyword + " Content: " + this->content;
}

QString ServerInfo::join()
{
    return this->keyword + ":" + this->content;
}
