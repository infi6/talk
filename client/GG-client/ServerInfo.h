#ifndef SERVERINFO_H
#define SERVERINFO_H

#include "Dependencies.h"

class ServerInfo
{
public:
    ServerInfo(QString message);
    ServerInfo(QString keyword, QString content);

    QString getKeyword();
    void setKeyword(QString keyword);
    QString getContent();
    void setContent(QString content);
    QString toString();
    QString join();

private:
    QString keyword;
    QString content;
};

#endif // SERVERINFO_H
