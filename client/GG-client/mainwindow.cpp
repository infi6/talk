#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent), ui(new Ui::MainWindow), connection(new Connection())
{
    ui->setupUi(this);
    ui->nicknameEditText->setText("mati");
    ui->ipEditText->setText("192.168.0.50");
    ui->passwordText->setText("test");
    ui->confirmPasswordText->setText("test");
    ui->portEditText->setText("1236");
    ui->confirmPasswordLabel->setEnabled(false);
    ui->confirmPasswordText->setEnabled(false);

    this->setFixedWidth(this->width());
    this->setFixedHeight(this->height());

    QObject::connect(connection, SIGNAL(changeConnectionStatus()), this, SLOT(changeConnectionStatus()));
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_connectDisconnectButton_clicked()
{
    if(!connection->getIsConnected())
    {
        QString nickname = ui->nicknameEditText->text();
        QString address = ui->ipEditText->text();
        QString port = ui->portEditText->text();
        QString password = ui->passwordText->text();
        QString confirmPassword = ui->confirmPasswordText->text();

        bool isNewUser = ui->newUserCheckBox->isChecked();

        if ((!isNewUser && ((nickname == "") || (address == "") || (port == "") || (password == ""))) ||
            (isNewUser && ((nickname == "") || (address == "") || (port == "") || (password == "") || (confirmPassword == ""))))
        {
            QMessageBox::critical(this, "Error!", "Please, fill all fields!");
        }
        else
        {
            if((isNewUser && (password == confirmPassword)) || !isNewUser)
            {
                connection->connect(address, port.toInt(), nickname, password, isNewUser);
            }
            else
            {
                QMessageBox::critical(this, "Error!", "Password are not the same!");
            }

        }
    }
    else
    {
        connection->disconnect("true");
        connection->logoutByUser();
    }
}

void MainWindow::changeConnectionStatus()
{
    if(connection->getIsConnected())
    {
        ui->connectDisconnectButton->setText("Logut");
    }
    else
    {
        ui->connectDisconnectButton->setText("Login");
    }
}

void MainWindow::on_newUserCheckBox_clicked()
{
    bool isChecked = ui->newUserCheckBox->isChecked();
    if(isChecked)
    {
        ui->confirmPasswordLabel->setEnabled(true);
        ui->confirmPasswordText->setEnabled(true);
    }
    else
    {
        ui->confirmPasswordLabel->setEnabled(false);
        ui->confirmPasswordText->setEnabled(false);
    }
}

void MainWindow::closeEvent(QCloseEvent *event)
{
    if(connection->getIsConnected())
    {
        connection->disconnect("true");
    }

    event->accept();
}
