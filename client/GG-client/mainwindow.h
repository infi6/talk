#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "Dependencies.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

protected:
    void closeEvent(QCloseEvent *event) override;

private slots:
    void on_connectDisconnectButton_clicked();
    void on_newUserCheckBox_clicked();

public slots:
    void changeConnectionStatus();

private:
    Ui::MainWindow *ui;
    Connection *connection;
};

#endif // MAINWINDOW_H
