#include "messengerwindow.h"
#include "ui_messengerwindow.h"

MessengerWindow::MessengerWindow(QWidget *parent, Connection *connection) :
    QDialog(parent),
    ui(new Ui::MessengerWindow)
{
    ui->setupUi(this);
    this->connection = connection;

    this->setWindowTitle("#talk");
    ui->loginUserLabel->setText(connection->getNickname());
    ui->messageListWidget->setEnabled(false);
    ui->messageEdit->setEnabled(false);
    ui->sendButton->setEnabled(false);
    isMessageEnabled = false;
    ui->charsLeftLabel->setText("140");

    QObject::connect(this->connection, SIGNAL(changeFriendStatus(QString, QString)), this, SLOT(changeFriendStatus(QString, QString)));
    QObject::connect(this->connection, SIGNAL(newMessage(QString, QString, QString)), this, SLOT(newMessage(QString, QString, QString)));
    QObject::connect(this->connection, SIGNAL(serverDisconnected()), this, SLOT(serverDisconnected()));
}

MessengerWindow::~MessengerWindow()
{
    delete ui;
}

void MessengerWindow::on_sendButton_clicked()
{
    QString nickname = connection->getNickname();
    QString recieverNickname = connection->getRecieverNickname();
    QString messageContent = ui->messageEdit->toPlainText();

    if(!messageContent.trimmed().isEmpty())
    {
        if(!messageContent.contains("#") && !messageContent.contains("~") && !messageContent.contains(":"))
        {
            MessageEncoder *encoder = new MessageEncoder();
            QString message = encoder->Encode({new ServerInfo("from", nickname), new ServerInfo("message", messageContent), new ServerInfo("to", recieverNickname)});

            connection->writeToServer(message);
            ui->messageEdit->clear();

            delete encoder;
        }
        else
        {
            QMessageBox::critical(this, "Error!", "You can't use: '#' '~' ':'!");
        }
    }
    else
    {
        QMessageBox::critical(this, "Error!", "Please enter the message!");
    }
}

void MessengerWindow::on_addFriendButton_clicked()
{
    QString nicknameToAdd = ui->lineEdit->text();

    if(nicknameToAdd != "")
    {
        for(int i = 0; i < ui->friendListWidget->count(); i++)
        {
            if(ui->friendListWidget->item(i)->text() == nicknameToAdd)
            {
                QMessageBox::critical(this, "Error!", "You already have this friend!");
                break;
            }
        }

        MessageEncoder *encoder = new MessageEncoder();
        QString message = encoder->Encode({new ServerInfo("nick", connection->getNickname()), new ServerInfo("addfriend", "true"), new ServerInfo("friend", nicknameToAdd)});

        connection->writeToServer(message);

        delete encoder;
    }
    else
    {
         QMessageBox::critical(this, "Error!", "Please, fill friend nickname field!");
    }
    ui->lineEdit->clear();
}

void MessengerWindow::on_friendListWidget_itemClicked(QListWidgetItem *item)
{
    QString recievierNickname = item->text();
    connection->setRecievierNickname(recievierNickname);
    ui->messageToUserLabel->setText(recievierNickname);

    ui->messageListWidget->setEnabled(true);
    ui->messageEdit->setEnabled(true);
    ui->sendButton->setEnabled(true);
    ui->messageListWidget->clear();
    isMessageEnabled = true;

    item->setBackground(Qt::white);

    MessageEncoder *encoder = new MessageEncoder();
    QString message = encoder->Encode({new ServerInfo("nick", connection->getNickname()), new ServerInfo("conversation", connection->getRecieverNickname())});

    connection->writeToServer(message);

    delete encoder;
}

void MessengerWindow::on_messageEdit_textChanged()
{
    QString text = ui->messageEdit->toPlainText();
    int charsLeft = 140 - text.length();
    QString charsLeftText = QString::number(charsLeft);
    ui->charsLeftLabel->setText(charsLeftText);

    if(text.length() > 0)
    {
        QChar last = text[text.length()-1];
        if(last == '\n')
        {
            ui->messageEdit->setText(text.left(text.length() - 1));
            emit on_sendButton_clicked();
        }
        else if(charsLeft < 0)
        {
            QMessageBox::critical(this, "Error!", "Max character reached!");
            ui->messageEdit->setText(text.left(text.length() - (text.length() - 140)));
        }
    }
}

void MessengerWindow::changeFriendStatus(QString status, QString nickname)
{
    if(status == "added")
    {
        ui->friendListWidget->addItem(nickname);
    }
    if(status == "oldfriend")
    {
        bool error = false;
        for(int i = 0; i < ui->friendListWidget->count(); i++)
        {
            if(ui->friendListWidget->item(i)->text() == nickname)
            {
                error = true;
                break;
            }
        }

        if(!error)
        {
            ui->friendListWidget->addItem(nickname);
        }
    }
    else if(status == "notadded")
    {
        QMessageBox::critical(this, "Error!", "User with nickname: " + nickname + " not exist!");
    }
}

void MessengerWindow::newMessage(QString from, QString message, QString to)
{
    QString actualNickname = connection->getNickname();
    QString actualRecieverNickname = connection->getRecieverNickname();

    if((from == actualRecieverNickname || to == actualRecieverNickname) && isMessageEnabled)
    {
        QString nickAndMessage = from + ": " + message;
        ui->messageListWidget->addItem(nickAndMessage);
        ui->messageListWidget->scrollToBottom();
    }
    else
    {
        QString reciever = (from == actualNickname) ? to : from;

        for(int i = 0; i < ui->friendListWidget->count(); i ++)
        {
            if(ui->friendListWidget->item(i)->text() == reciever)
            {
                ui->friendListWidget->item(i)->setBackground(Qt::green);
            }
        }
    }
}

void MessengerWindow::serverDisconnected()
{
    QMessageBox::critical(this, "Error!", "Server is disconnected sorry! :(");
    this->destroy();
}
