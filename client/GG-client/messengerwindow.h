#ifndef MESSENGERWINDOW_H
#define MESSENGERWINDOW_H

#include <QDialog>
#include "Dependencies.h"
#include "ui_messengerwindow.h"

class Connection;

namespace Ui {
class MessengerWindow;
}

class MessengerWindow : public QDialog
{
    Q_OBJECT

public:
    explicit MessengerWindow(QWidget *parent = 0, Connection *connection = 0);
    ~MessengerWindow();

private slots:
    void on_sendButton_clicked();
    void on_addFriendButton_clicked();
    void on_friendListWidget_itemClicked(QListWidgetItem *item);
    void on_messageEdit_textChanged();

private:
    Ui::MessengerWindow *ui;
    Connection *connection;
    bool isMessageEnabled;

public slots:
    void changeFriendStatus(QString status, QString nickname);
    void newMessage(QString from, QString message, QString to);
    void serverDisconnected();
};

#endif // MESSENGERWINDOW_H
