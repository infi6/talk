//
// Created by filowsky on 13.01.18.
//

#include "Conversation.h"

User *Conversation::getConverser() const {
    return converser;
}

void Conversation::setConverser(User *converser) {
    Conversation::converser = converser;
}

const vector<Message *> &Conversation::getMessages() const {
    return messages;
}

void Conversation::setMessages(const vector<Message *> &messages) {
    Conversation::messages = messages;
}
