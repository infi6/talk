//
// Created by filowsky on 13.01.18.
//

#ifndef GG_SERVER_CONVERSATION_H
#define GG_SERVER_CONVERSATION_H


#include <vector>
#include "User.h"
#include "Message.h"

class User;

class Conversation {
private:
    User *converser;
    vector<Message *> messages;
public:
    const vector<Message *> &getMessages() const;

    void setMessages(const vector<Message *> &messages);

    User *getConverser() const;

    void setConverser(User *converser);
};


#endif //GG_SERVER_CONVERSATION_H
