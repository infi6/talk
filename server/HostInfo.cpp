//
// Created by filowsky on 08.01.18.
//

#include "HostInfo.h"

#include <utility>

HostInfo::HostInfo(string keyword, string content) {
    this->keyword = std::move(keyword);
    this->content = std::move(content);
}

HostInfo::HostInfo() {}

string HostInfo::getKeyword() {
    return this->keyword;
}

void HostInfo::setKeyword(string keyword) {
    this->keyword = std::move(keyword);
}

string HostInfo::getContent() {
    return this->content;
}

void HostInfo::setContent(string content) {
    this->content = std::move(content);
}
