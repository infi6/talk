//
// Created by filowsky on 08.01.18.
//

#ifndef GG_SERVER_HOSTINFO_H
#define GG_SERVER_HOSTINFO_H

#include <array>

using namespace std;

class HostInfo {
private:
    string keyword;
    string content;
public:
    HostInfo(string keyword, string content);

    HostInfo();

    string getKeyword();

    void setKeyword(string keyword);

    string getContent();

    void setContent(string content);
};


#endif //GG_SERVER_HOSTINFO_H
