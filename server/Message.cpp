//
// Created by filowsky on 13.01.18.
//

#include "Message.h"

const string &Message::getContent() const {
    return content;
}

void Message::setContent(const string &content) {
    Message::content = content;
}

char *Message::getDt() const {
    return dt;
}

void Message::setDt(char *dt) {
    Message::dt = dt;
}

const string &Message::getFrom() const {
    return from;
}

void Message::setFrom(const string &from) {
    Message::from = from;
}

const string &Message::getTo() const {
    return to;
}

void Message::setTo(const string &to) {
    Message::to = to;
}
