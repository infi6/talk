//
// Created by filowsky on 13.01.18.
//

#ifndef GG_SERVER_MESSAGE_H
#define GG_SERVER_MESSAGE_H

#include <string>
#include <iostream>
#include <ctime>

using namespace std;

class Message {
private:
    string content;
    time_t now = time(nullptr);
    tm *gmtm = gmtime(&now);
    char *dt = asctime(gmtm);
    string from;
    string to;
public:
    const string &getFrom() const;

    void setFrom(const string &from);

    const string &getTo() const;

    void setTo(const string &to);

    const string &getContent() const;

    void setContent(const string &content);

    char *getDt() const;

    void setDt(char *dt);
};


#endif //GG_SERVER_MESSAGE_H
