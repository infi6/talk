//
// Created by filowsky on 13.01.18.
//

#include <cstring>
#include "ProtocolParser.h"

std::vector<HostInfo *> ProtocolParser::getHostInfos(std::vector<std::string> keywordsWithContent) {
    string keywordBuffer;
    string contentBuffer;
    vector<HostInfo *> hostInfos;

    for (int i = 0; i < keywordsWithContent.size(); i++) {
        for (int j = 0; j < keywordsWithContent[i].size(); j++) {
            if (keywordsWithContent[i][j] != ':') {
                keywordBuffer += keywordsWithContent[i][j];
            } else {
                auto *hostInfo = new HostInfo();
                hostInfo->setKeyword(keywordBuffer);
                keywordBuffer.clear();
                for (int k = j + 1; k < keywordsWithContent[i].size(); k++) {
                    contentBuffer += keywordsWithContent[i][k];
                }
                hostInfo->setContent(contentBuffer);
                contentBuffer.clear();
                hostInfos.push_back(hostInfo);
                break;
            }
        }
    }
    return hostInfos;
}

std::vector<std::string> ProtocolParser::getKeywordsWithContent(char *buffer) {
    string contentWithKeyword;
    vector<string> keywordAndContent;

    if (buffer[0] == '#') {
        for (int i = 1; i < strlen(buffer); i++) {
            if (buffer[i] != '#') {
                contentWithKeyword += buffer[i];
            } else {
                keywordAndContent.push_back(contentWithKeyword);
                contentWithKeyword.clear();
            }
        }
    }
    return keywordAndContent;
}