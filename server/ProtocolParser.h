//
// Created by filowsky on 13.01.18.
//

#ifndef GG_SERVER_PROTOCOLPARSER_H
#define GG_SERVER_PROTOCOLPARSER_H

#include <vector>
#include "HostInfo.h"

using namespace std;

class ProtocolParser {
public:
    std::vector<HostInfo *> getHostInfos(std::vector<std::string> keywordsWithContent);

    std::vector<std::string> getKeywordsWithContent(char buffer[]);

};


#endif //GG_SERVER_PROTOCOLPARSER_H
