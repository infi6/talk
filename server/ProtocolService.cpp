//
// Created by filowsky on 13.01.18.
//

#include <unistd.h>
#include <cstring>
#include "ProtocolService.h"

void ProtocolService::serveProtocolForLogging(vector<User *> *users, User *th_data, vector<HostInfo *> hostInfos) {
    string nickname = hostInfos[0]->getContent();
    string action = hostInfos[1]->getKeyword();
    string messageToClient;
    // hostInfos[2] ->> if newuser - true/false

    if (action == "pass") {
        string password = hostInfos[1]->getContent();
        if (hostInfos[2]->getContent() == "true") {
            if (ifUserAlreadyExists(*users, nickname)) {
                messageToClient = "#nick:" + nickname + "#status:exist#~";
                write(th_data->getConnection_socket(), messageToClient.c_str(), messageToClient.length());
            } else {
                messageToClient = "#nick:" + nickname + "#status:successful#~";
                th_data->setPassword(password);
                th_data->setNickname(nickname);
                th_data->setIsActive(true);
                users->push_back(th_data);
                write(th_data->getConnection_socket(), messageToClient.c_str(), messageToClient.length());
            }
        } else {
            User *user = findUserByNickname(*users, nickname);
            if (ifAlreadyLogged(*users, nickname)) {
                messageToClient = "#nick:" + nickname + "#status:alreadylogged#~";
                write(th_data->getConnection_socket(), messageToClient.c_str(), messageToClient.length());
            } else if (!ifAlreadyLogged(*users, nickname) && ifPasswordIsCorrect(*users, password, nickname)) {
                messageToClient = "#nick:" + nickname + "#status:successful#~";
                user->setConnection_socket(th_data->getConnection_socket());
                user->setIsActive(true);
                write(user->getConnection_socket(), messageToClient.c_str(), messageToClient.length());
                if (user->getFriends().size() > 0) {
                    string message;
                    for (int i = 0; i < user->getFriends().size(); ++i) {
                        message.append("#nick:" + user->getNickname() + "#addfriend:oldfriend#friend:" +
                                       user->getFriends()[i]->getNickname() + "#~");
                    }
                    write(user->getConnection_socket(), message.c_str(), message.length());
                }
            } else if (!ifPasswordIsCorrect(*users, password, nickname)) {
                messageToClient = "#nick:" + nickname + "#status:wrongpassword#~";
                write(th_data->getConnection_socket(), messageToClient.c_str(), messageToClient.length());
            }
        }
    } else if (action == "disconnect") {
        if (hostInfos[1]->getContent() == "true") {
            User *user = findUserByNickname(*users, nickname);
            if (users->size() > 0) {
                messageToClient = "#nick:" + nickname + "#status:disconnected#~";
                user->setIsActive(false);
                write(user->getConnection_socket(), messageToClient.c_str(), messageToClient.length());
            }
        }
        //terminate();
    } else if (action == "addfriend" && hostInfos[1]->getContent() == "true") {
        string friendsNick = hostInfos[2]->getContent();
        User *user = findUserByNickname(*users, nickname);
        User *converser = findUserByNickname(*users, friendsNick);
        if (isAbleToAddFriend(*users, user, friendsNick)) {
            messageToClient = "#nick:" + nickname + "#addfriend:added" + "#friend:" + friendsNick + "#~";

            // czy tak zadziała dodanie frienda? czy jest inny prostszy sposób?
            addFriendAndConversation(user, converser);
            addFriendAndConversation(converser, user);
            if (converser->isIsActive()) {
                string message;
                for (auto i : converser->getFriends()) {
                    message.append("#nick:" + converser->getNickname() + "#addfriend:oldfriend#friend:" +
                                   i->getNickname() + "#~");
                }
                write(converser->getConnection_socket(), message.c_str(), message.length());
            }

            cout << "================================================================\n";
            for (int i = 0; i < user->getFriends().size(); ++i) {
                cout << "User " << user->getNickname() << "friends:\n";
                cout << "friend's nickname: " << user->getFriends()[i]->getNickname() << "\n";
                cout << "friend's password: " << user->getFriends()[i]->getPassword() << "\n";
            }
            cout << "================================================================\n";

            write(user->getConnection_socket(), messageToClient.c_str(), messageToClient.length());
        } else {
            messageToClient = "#nick:" + nickname + "#addfriend:notadded" + "#friend:" + friendsNick + "#~";
            write(user->getConnection_socket(), messageToClient.c_str(), messageToClient.length());
        }
    }
}

void ProtocolService::serveProtocolForSendingMessages(
        vector<User *> *users, User *th_data, vector<HostInfo *> hostInfos, char *buffer) {
    string userFromNick = hostInfos[0]->getContent();
    string userToNick = hostInfos[2]->getContent();
    string message = hostInfos[1]->getContent();

    User *userFrom = findUserByNickname(*users, userFromNick);
    User *userTo = findUserByNickname(*users, userToNick);

    spreadMessage(userFrom, userTo, message);

    string msg = buffer;
    msg.append("~");
    cout << "msg: " << msg << "\n";

    if (userFrom->isIsActive()) {
        write(userFrom->getConnection_socket(), msg.c_str(), msg.length());
    }

    if (userTo->isIsActive()) {
        write(userTo->getConnection_socket(), msg.c_str(), msg.length());
    }
}

void ProtocolService::sendOldMessages(vector<User *> *users, const string &nickFrom, const string &nickTo) {
    User *user = findUserByNickname(*users, nickFrom);
    Conversation *conversation = findConversation(nickTo, user);
    string message;
    for (auto msg : conversation->getMessages()) {
        message.append("#from:" + msg->getFrom() + "#message:" +
                       msg->getContent() + "#to:" + msg->getTo() + "#~");
    }
    write(user->getConnection_socket(), message.c_str(), message.length());
}

bool ProtocolService::isAbleToAddFriend(vector<User *> users, User *userWhoAddsFriend, const string &friendsNickname) {
    if (userWhoAddsFriend->getNickname() != friendsNickname) {
        for (auto &user : users) {
            if (user->getNickname() == friendsNickname) {
                for (auto &userFriend : userWhoAddsFriend->getFriends()) {
                    if (userFriend->getNickname() == friendsNickname) {
                        cout << "Unable to add this friend again!\n";
                        return false;
                    }
                }
                cout << "Able to add friend " << friendsNickname << "\n";
                return true;
            }
        }
        cout << "There is no such user with nickname: " << friendsNickname << "\n";
        return false;
    } else {
        cout << "You cannot add yourself to friends list!\n";
        return false;
    }
}

bool ProtocolService::ifPasswordIsCorrect(vector<User *> users, const string &password, const string &nickname) {
    for (auto &user : users) {
        if (user->getNickname() == nickname && user->getPassword() == password) {
            return true;
        }
    }
    return false;
}

bool ProtocolService::ifUserAlreadyExists(vector<User *> users, const string &nickname) {
    for (auto &user : users) {
        if (user->getNickname() == nickname) {
            cout << "Given nickname is already taken\n";
            return true;
        }
    }
    return false;
}

bool ProtocolService::ifAlreadyLogged(vector<User *> users, const string &nickname) {
    for (auto &user : users) {
        if (user->getNickname() == nickname && user->isIsActive()) {
            cout << "User is already logged in!\n";
            return true;
        }
    }
    return false;
}

User *ProtocolService::findUserByNickname(vector<User *> users, string nickname) {
    for (auto &user : users) {
        if (user->getNickname() == nickname)
            return user;
    }
    return nullptr;
}

Conversation *ProtocolService::findConversation(const string &converserNick, User *user) {
    vector<Conversation *> conversations = user->getConversations();
    for (auto &conversation : conversations) {
        if (conversation->getConverser()->getNickname() == converserNick)
            return conversation;
    }
    return nullptr;
}

void ProtocolService::addFriendAndConversation(User *from, User *to) {
    vector<User *> friends = from->getFriends();
    friends.push_back(to);
    from->setFriends(friends);

    vector<Conversation *> conversations = from->getConversations();
    auto *conversation = new Conversation;
    conversation->setConverser(to);
    conversations.push_back(conversation);
    from->setConversations(conversations);
}

//rozsyłanie wiadomości zarówno do nadawcy jak i odbiorcy
void ProtocolService::spreadMessage(User *from, User *to, const string &message) {
    Conversation *userFromConversation = findConversation(to->getNickname(), from);
    Conversation *userToConversation = findConversation(from->getNickname(), to);

    auto *msg = new Message;
    msg->setContent(message);
    msg->setFrom(from->getNickname());
    msg->setTo(to->getNickname());

    vector<Message *> userFromMessages = userFromConversation->getMessages();
    userFromMessages.push_back(msg);
    userFromConversation->setMessages(userFromMessages);

    vector<Message *> userToMessages = userToConversation->getMessages();
    userToMessages.push_back(msg);
    userToConversation->setMessages(userFromMessages);
}