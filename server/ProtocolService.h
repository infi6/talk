//
// Created by filowsky on 13.01.18.
//

#ifndef GG_SERVER_PROTOCOLSERVICE_H
#define GG_SERVER_PROTOCOLSERVICE_H


#include "HostInfo.h"
#include "User.h"

using namespace std;

class ProtocolService {
public:
    void serveProtocolForLogging(vector<User *> *users, User *th_data, vector<HostInfo *> hostInfos);

    void
    serveProtocolForSendingMessages(vector<User *> *users, User *th_data, vector<HostInfo *> hostInfos, char *buffer);

    void sendOldMessages(vector<User *> *users, const string &nickFrom, const string &nickTo);

private:
    bool isAbleToAddFriend(vector<User *> users, User *user, const string &friendsNickname);

    bool ifPasswordIsCorrect(vector<User *> users, const string &password, const string &nickname);

    bool ifUserAlreadyExists(vector<User *> users, const string &nickname);

    bool ifAlreadyLogged(vector<User *> users, const string &nickname);

    User *findUserByNickname(vector<User *> users, string nickname);

    Conversation *findConversation(const string &converserNick, User *user);

    void addFriendAndConversation(User *from, User *to);

    void spreadMessage(User *from, User *to, const string &message);
};

#endif //GG_SERVER_PROTOCOLSERVICE_H