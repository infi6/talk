//
// Created by filowsky on 04.01.18.
//

#include "User.h"


bool User::isIsActive() const {
    return isActive;
}

void User::setIsActive(bool isActive) {
    User::isActive = isActive;
}

const vector<User *> &User::getFriends() const {
    return friends;
}

void User::setFriends(const vector<User *> &friends) {
    User::friends = friends;
}

const vector<Conversation *> &User::getConversations() const {
    return conversations;
}

void User::setConversations(const vector<Conversation *> &conversations) {
    User::conversations = conversations;
}

int User::getConnection_socket() const {
    return connection_socket;
}

void User::setConnection_socket(int connection_socket) {
    User::connection_socket = connection_socket;
}

const string &User::getPassword() const {
    return password;
}

void User::setPassword(const string &password) {
    User::password = password;
}

const string &User::getNickname() const {
    return nickname;
}

void User::setNickname(const string &nickname) {
    User::nickname = nickname;
}
