//
// Created by filowsky on 04.01.18.
//

#ifndef GG_SERVER_USER_H
#define GG_SERVER_USER_H

#include <string>
#include <iostream>
#include <vector>
#include "Conversation.h"

class Conversation;

using namespace std;

class User {
private:
    string nickname;
    string password;
    int connection_socket;
    bool isActive;
    vector<User *> friends;
    vector<Conversation *> conversations;
public:
    bool isIsActive() const;

    void setIsActive(bool isActive);

    const vector<User *> &getFriends() const;

    void setFriends(const vector<User *> &friends);

    const vector<Conversation *> &getConversations() const;

    void setConversations(const vector<Conversation *> &conversations);

    int getConnection_socket() const;

    void setConnection_socket(int connection_socket);

    const string &getPassword() const;

    void setPassword(const string &password);

    const string &getNickname() const;

    void setNickname(const string &nickname);
};


#endif //GG_SERVER_USER_H
