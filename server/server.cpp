#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <pthread.h>
#include <mutex>
#include "User.h"
#include "HostInfo.h"
#include "ProtocolParser.h"
#include "ProtocolService.h"

using namespace std;

#define SERVER_PORT 1236
#define QUEUE_SIZE 5
#define BUFFER_SIZE 500

vector<User *> users;
ProtocolParser *protocolParser = new ProtocolParser();
ProtocolService *protocolService = new ProtocolService();

void *ThreadBehavior(void *user) {
    char buffer[BUFFER_SIZE];
    int responseLength;
    mutex service_mutex;

    pthread_detach(pthread_self());

    auto *th_data = (User *) user;
    char *msg;

    while (1) {
        responseLength = static_cast<int>(read(th_data->getConnection_socket(), buffer, BUFFER_SIZE));

        if (responseLength > 0) {

            buffer[responseLength] = '\0';
            msg = strtok(buffer, "~");
            char message[strlen(msg)];
            strcpy(message, msg);

            vector<string> keyWordsWithContent = protocolParser->getKeywordsWithContent(buffer);
            vector<HostInfo *> hostInfos = protocolParser->getHostInfos(keyWordsWithContent);


            for (auto &hostInfo : hostInfos) {
                cout << "keyword: " << hostInfo->getKeyword() << '\n'
                     << "content: " << hostInfo->getContent() << '\n';
            }
            cout << '\n';


            service_mutex.try_lock();

            if (hostInfos[1]->getKeyword() == "message")
                protocolService->serveProtocolForSendingMessages(&users, th_data, hostInfos, buffer);
            else if (hostInfos[1]->getKeyword() == "conversation") {
                string nickTo = hostInfos[1]->getContent();
                string nickFrom = hostInfos[0]->getContent();
                protocolService->sendOldMessages(&users, nickFrom, nickTo);
            } else
                protocolService->serveProtocolForLogging(&users, th_data, hostInfos);

            service_mutex.unlock();

            for (auto &user : users) {
                cout << "username: " << user->getNickname() << "\n";
                cout << "password: " << user->getPassword() << "\n";
                cout << '\n';
            }
        }
    }
    pthread_exit(NULL);
}

void handleConnection(int connection_socket_descriptor) {
    //wynik funkcji tworząca wątek
    int create_result = 0;

    //uchwyt na wątek
    pthread_t thread1;

    User *user = new User();
    user->setConnection_socket(connection_socket_descriptor);

    create_result = pthread_create(&thread1, NULL, ThreadBehavior, (void *) user);

    if (create_result) {
        printf("Błąd przy próbie utworzenia wątku, kod błędu: %d\n", create_result);
        exit(-1);
    }
}

int main(int argc, char *argv[]) {
    int server_socket_descriptor;
    int connection_socket_descriptor;
    int bind_result;
    int listen_result;
    char reuse_addr_val = 1;
    struct sockaddr_in server_address{};
    mutex connection_mutex;

    //inicjalizacja gniazda serwera
    memset(&server_address, 0, sizeof(struct sockaddr));
    server_address.sin_family = AF_INET;
    server_address.sin_addr.s_addr = htonl(INADDR_ANY);
    server_address.sin_port = htons(SERVER_PORT);

    server_socket_descriptor = socket(AF_INET, SOCK_STREAM, 0);
    if (server_socket_descriptor < 0) {
        fprintf(stderr, "%s: Błąd przy próbuie utworzenia gniazda..\n", argv[0]);
        exit(1);
    }
    setsockopt(server_socket_descriptor, SOL_SOCKET, SO_REUSEADDR, (char *) &reuse_addr_val, sizeof(reuse_addr_val));

    bind_result = bind(server_socket_descriptor, (struct sockaddr *) &server_address, sizeof(struct sockaddr));
    if (bind_result < 0) {
        fprintf(stderr, "%s: Błąd przy próbie dowiązania adresu IP i numeru portu do gniazda.\n", argv[0]);
        exit(1);
    }

    listen_result = listen(server_socket_descriptor, QUEUE_SIZE);
    if (listen_result < 0) {
        fprintf(stderr, "%s: Błąd przy próbie ustawienia wielkości kolejki.\n", argv[0]);
        exit(1);
    }

    while (1) {
        connection_mutex.try_lock();

        connection_socket_descriptor = accept(server_socket_descriptor, NULL, NULL);

        if (connection_socket_descriptor < 0) {
            fprintf(stderr, "%s: Błąd przy próbie utworzenia gniazda dla połączenia.\n", argv[0]);
            exit(1);
        }

        connection_mutex.unlock();

        handleConnection(connection_socket_descriptor);
    }

    connection_mutex.try_lock();
    close(server_socket_descriptor);
    connection_mutex.unlock();
    return (0);
}